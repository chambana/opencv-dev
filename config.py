
########################## IMAGE PROCESSING CONSTANTS ##############
BLUR_KERNEL_SIZE = 21     #helps make uniform blobs of green
ERODE_DILATE_KERNAL_SIZE = 5
MIN_CONTOUR_AREA = 500            #minimum area (in pixels) of a contour (blob) to not be considered false positive
REGION_OF_INTEREST = 0.5  #percentage of the frame used to search for things 0.1-1.0
WIDTH_FRAME = 640
HEIGHT_FRAME = 480
RED_SENSITIVITY = 25
RED_HUE_LOWER_RANGE = 0
RED_HUE_UPPER_RANGE = 180
GREEN_SENSITIVITY = 25
GREEN_HUE = 60
# WIDTH_FRAME = int(cap.get(3))
# HEIGHT_FRAME = int(cap.get(4))

#ENUMERATE DIRECTIONAL CONSTANTS
DIRECTION = {
    'FORWARD_YAW':'FORWARD',
    'RIGHT_YAW':'RIGHT',
    'BACKWARD_YAW':'BACKWARD',
    'LEFT_YAW':'LEFT',
    'UNKNOWN_YAW':'UNKNOWN'
}

