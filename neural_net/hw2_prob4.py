from math import *
import sys


###
#Good set of weights for  w0,w1,w2,w3,w4,c1,c2,c3
#Weights:                -2 0 2 0 0 -3 0 -3
#Minimum Error:           0.222989639862
###

#w0,w1,w2,w3,w4
w0=1
w1=1
w2=1
w3=1
w4=1

#c1,c2,c3
c1=1
c2=1
c3=1


def g(x, c):
    #G(x) function.  Doesn't handle Divide by 0, handled elsewhere
    return ((x**2.0) / ((x**2.0) + (c**2.0)))


def r_calc(x):
    #calculate R from the current set of global weights and input args x[1-4]
    r = w0 + w1 * g(x[1],c1) + w2 * g(x[2],c2) + w3 * g(x[3],c3) + w4 * x[4]
    # print w1 * g(x[1],c1)
    # print w2 * g(x[2],c2)
    # print w3 * g(x[3],c3)
    # print w4 * x[4]
    #print "R=", r
    return r

def process_list(longlist):
    #Takes the file data, assumes it's already been split and stripped of whitespace, and converts each row
    # to 4 corresponding X values, x[1-4].  Makes a new list with the same length as the file data, but containing just
    # the corresponding X values for each row of file data
    processed = []
    for index, value in enumerate(longlist):
        row=[]
        row.append(None)  #dummy value.  for readability only
        row.append(len(value[1].replace(' ','')))
        row.append(int(value[2]))
        row.append(int(value[3]))
        row.append(1 if value[1][0].isupper() else 0)
        processed.append(row)
    return processed

def abs_error_calc(raw_data, x_data):
    #calcs the absolute value of the error for a set of weights, which is how we judge how "good" a set of weights is.
    #lower the better.
    N=len(raw_data)
    sum=0
    for i in range(0,N):
        r_i = r_calc(x_data[i])
        t_i = 1 if raw_data[i][0]=='u' else 0
        #print r_i, t_i
        sum+=abs(r_i-t_i)
    return (1.0/N)*sum

def optimize_weights(data, x_vals):
    #automated way to find a good set of weights.  This problem explodes quickly, so practical weight ranges are pretty limited.
    global w0, w1, w2, w3, w4, c1, c2, c3
    minimum_error = sys.float_info.max
    weights_lower_bound = -2
    weights_upper_bound = 2

    for w_zero in range(weights_lower_bound, weights_upper_bound, 1):
        print "progress canary:", w_zero
        for w_one in range(weights_lower_bound, weights_upper_bound, 1):
            for w_two in range(weights_lower_bound,weights_upper_bound,1):
                for w_three in range (weights_lower_bound,weights_upper_bound,1):
                    for w_four in range (weights_lower_bound,weights_upper_bound,1):
                        for c_one in range(weights_lower_bound,weights_upper_bound,1):
                            for c_two in range(weights_lower_bound,weights_upper_bound,1):
                                for c_three in range(weights_lower_bound,weights_upper_bound,1):
                                    w0=w_zero
                                    w1=w_one
                                    w2=w_two
                                    w3=w_three
                                    w4=w_four
                                    c1=c_one
                                    c2=c_two
                                    c3=c_three
                                    try:
                                        error = abs_error_calc(data, x_vals)
                                        #print w0,w1,w2,w3,w4,c1,c2,c3, "error=", error
                                        if error<minimum_error:
                                            minimum_error=error
                                            print "Minimum Error:", minimum_error
                                            print "Weights:", w0,w1,w2,w3,w4,c1,c2,c3

                                    except:
                                        pass
                                        #print "Div by 0, skipping"
    print "Minimum Error was", minimum_error


if __name__ == "__main__":

    print "******** hw2, prob4 **********"

    # INPUT DATA FORMAT:
    # ['u', 'm geller', '5', '16']
    #  interesting (u or n), name, num of drives found on, min of the counts on the file names of the 2 name words

    datalist = []
    file=open("data.txt","r")

    #Grab the data from the file, strip the newline, and split it using the pipe delimiter
    for line in file:
        datalist.append(line.rstrip('\n').split("|"))

    print "DEBUG: Parsed Data is:", datalist
    list_of_x_values = process_list(datalist)
    print "DEBUG:  Corresponding X values are: ", list_of_x_values

    # for index, x in enumerate(list_of_x_values):
    #     print "\nIndex: ", index
    #     print "processing data:",datalist[index]
    #     print "x values:",x
    #     print "R =",r_calc(x)
    #error = abs_error_calc(datalist, list_of_x_values)
    #print "Absolute Average Error: ", error


    optimize_weights(datalist, list_of_x_values)
    file.close()




