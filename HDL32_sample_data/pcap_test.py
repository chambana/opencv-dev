from scapy.all import *
import time
import struct
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from math import *
import numpy as np




input_file = 'HDL32-V2_Tunnel.pcap'
#input_file =  'HDL32-V2_Monterey Highway.pcap'

#Note: Laser #9 from the top of the stack, or DSR #15, is set at a 0 vertical angle. This laser can be used as a reference to calibrate pitch and
#yaw of the sensor relative to the vehicle.
'''The information from a single Firing Sequences of
32 lasers is contained in one (1) Data Block.
• Each packet contains the data from 12 Firing
Sequences.
• Only a single azimuth is returned per Data Block.'''

#It's a Right Hand coordinate system.  The coord is +X and the right hand side fo the cord is +Y.  +Z is up.
#The Azimuth is measured CW off the right side of the cord.  Forward (with cord in the back) is 270.



class HDL32eParser(object):
    def __init__(self, file=None):
        self.graph_x = []
        self.graph_y = []
        self.graph_z = []
        self.graph_x_2D = []
        self.graph_y_2D = []
        self.pcap_reader = PcapReader(file)
        self.temp = (                  -30.67, #0
                                       -9.33,  #1
                                       -29.33, #2
                                       -8.00,  #3
                                       -28.00, #4
                                       -6.67,  #5
                                       -26.67, #6
                                       -5.33,  #7
                                       -25.33, #8
                                       -4.00,  #9
                                       -24.00, #10
                                       -2.67,  #11
                                       -22.67, #12
                                       -1.33,  #13
                                       -21.33, #14
                                       -0.00,  #15
                                       -20.00, #16
                                       1.33,   #17
                                       -18.67, #18
                                       2.67,   #19
                                       -17.33, #20
                                       4.00,   #21
                                       -16.00, #22
                                       5.33,   #23
                                       -14.67, #24
                                       6.67,   #25
                                       -13.33, #26
                                       8.00,   #27
                                       -12.00, #28
                                       9.33,   #29
                                       -10.67, #30
                                       10.67)  #31
        self.laser_orientation_0_31=[]
        for laser_angle_degrees in self.temp:
            self.laser_orientation_0_31.append(radians(laser_angle_degrees))

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection='3d')

    def get_data_block(self, packet_payload, data_block_num_0_11):

        #12 datablocks starting at byte 0.
        #each block is:
        #   2 bytes flag
        #   2 bytes Azimuth
        #   32x    (2 bytes distance + 1 byte reflectivity)
        #
        # 12 datablocks * 100 bytes per datablock = 1200 bytes total in a packet


        try:
            data_block_first_byte = data_block_num_0_11 * 100

            flag = packet_payload[data_block_first_byte+0:data_block_first_byte+2]      # 2 byte flags (bytes 0,1)
            azimuth = packet_payload[data_block_first_byte+2:data_block_first_byte+4]   # 2 byte Aazimuth (bytes 2,3)
            laser_data = packet_payload[data_block_first_byte+4:data_block_first_byte+100]  #96 bytes for 32 laser channels
            return (flag, azimuth, laser_data)

        except Exception as e:
            print("exception: ", str(e))
            return None

    def read_packet(self, debug=False):
        counter = 0
        p = self.pcap_reader.read_packet()
        # payload 1 is ethernet frame, payload 2 IP layer, payload 3 is UDP layer, payload 4 is our raw payload

        packet_lidar_payload = bytes(p.payload.payload.payload)
        if debug:
            for byte in packet_lidar_payload:
                print(counter, byte, end=' ')
                counter += 1
            print('\n')
        return packet_lidar_payload


    def get_azimuth(self, datablock):
        '''the azimuth is an integer 0-35999 that's represented as 2 bytes.  Uses unpack to convert to short (2 byte int)'''

        azimuth_bytes = datablock[1]
        # print(struct.unpack('i', azimuth+ b'\x00\x00')[0])
        azimuth = struct.unpack('H', azimuth_bytes)[0] / 100
        return azimuth

    def get_flag(self, datablock):
        #TODO:  no clue what the format of this should be.  PLACEHOLDER
        flag_bytes = datablock[0]
        flag = struct.unpack('H', flag_bytes)[0]
        return flag

    def get_laser_data(self, datablock, laser_number_0_31):
        '''laser data blocks start after 2+2 bytes of flag+azimuth
         0-31 laser blocks each have 3 bytes of data each:  2 bytes distance and 1 byte reflectivity'''

        laser_data = datablock[2]
        laser_data_i_start_byte = laser_number_0_31*3

        laser_i_distance_raw = laser_data[laser_data_i_start_byte:laser_data_i_start_byte+2]
        laser_i_reflectivity_raw = laser_data[laser_data_i_start_byte+2:laser_data_i_start_byte+3]

        laser_i_distance = struct.unpack('H',laser_i_distance_raw)[0]
        laser_i_reflectivity = struct.unpack('B',laser_i_reflectivity_raw)[0]

        return (laser_i_distance, laser_i_reflectivity)

    def graph_add_point_2D(self,azimuth,distance):
        ##self.plt.scatter(x,y)

        #self.graph_x.append(x)
        #self.graph_y.append(y)
        self.graph_x_2D.append(distance*sin(radians(azimuth)))
        self.graph_y_2D.append(distance*cos(radians(azimuth)))

    def graph_add_point_3D(self, x, y, z):

        self.graph_x.append(x)
        self.graph_y.append(y)
        self.graph_z.append(z)


    def graph_show_3D(self):
        self.ax.scatter(self.graph_x, self.graph_y, self.graph_z, c='r', marker='o')

        self.ax.set_xlabel('X Label')
        self.ax.set_ylabel('Y Label')
        self.ax.set_zlabel('Z Label')
        #self.fig.hold()
        plt.show(block=True)

        #time.sleep(10)
        #plt.close()


    def graph_show_2D(self):
        print("plotting", len(self.graph_x),"points")
        plt.scatter(self.graph_x_2D,self.graph_y_2D)
        plt.show()



    def create_3d_point(self, laser_number_0_31, distance, azimuth):
        #HDL32 is RHS with +y in forward direction
        #use XYZ fixed angle rotation.  First Vertical_Angle around X, then 0 around y, then NEGATIVE Azimuth around Z
        vertical_angle = self.laser_orientation_0_31[laser_number_0_31]
        azimuth = radians(azimuth)

        #gamma=x = vertical angle
        #beta = y = 0
        #alpha= NEGATIVE azimuth
        transform_xyz = np.array([[cos(-azimuth)*cos(0), cos(-azimuth)*sin(0)*sin(vertical_angle)-sin(-azimuth)*cos(vertical_angle),     cos(-azimuth)*sin(0)*cos(vertical_angle)+sin(-azimuth)*sin(vertical_angle)],
                                       [sin(-azimuth)*cos(0),     sin(-azimuth)*sin(0)*sin(vertical_angle) + cos(-azimuth)*cos(vertical_angle),   sin(-azimuth)*sin(0)*cos(vertical_angle) - cos(-azimuth)*sin(vertical_angle)],
                                       [-sin(0),          cos(0)*sin(vertical_angle),                        cos(0)*cos(vertical_angle)],
                                       ])
        point_a_frame = np.array([[0],
                                  [distance],
                                  [0]])

        point_b_frame = np.dot(transform_xyz, point_a_frame)


        return (point_b_frame[0], point_b_frame[1], point_b_frame[2])



        # vertical_angle = self.laser_orientation_0_31[laser_number_0_31]
        #
        # azimuth = radians(azimuth)
        # transform_aprime_a = np.array([ [cos(azimuth),  sin(azimuth), 0, 0] ,
        #                                 [-sin(azimuth), cos(azimuth), 0, 0],
        #                                 [0,              0,           1, 0],
        #                                 [0,              0,           0, 1]  ])
        # point_a_frame = np.array([[0],
        #                           [distance],
        #                           [0],
        #                           [1]])
        #
        #
        # point_aprime_frame = np.dot(transform_aprime_a, point_a_frame)
        #
        # transform_b_aprime = np.array([ [1,      0,                    0,                0],
        #                                 [0, cos(vertical_angle), -sin(vertical_angle),   0],
        #                                 [0, sin(vertical_angle),  cos(vertical_angle),   0],
        #                                 [0,      0,                    0,                1]  ])
        #
        # point_b_frame = np.dot(transform_b_aprime, point_aprime_frame)
        # return (point_b_frame[0], point_b_frame[1], point_b_frame[2])
        #


print("LIDAR testing")



lidar_parser = HDL32eParser(file=input_file)

starttime = time.time()

#print(lidar_parser.create_3d_point(laser_number_0_31=15, distance=5, azimuth=45+180))
#quit()

one_rotation = False
start_azimuth = None
for packet in range(10000,11000, 1):
    if one_rotation==True:
        break
    ##print("Packet "+str(packet)+":")
    test_packet = lidar_parser.read_packet(debug=False)
    if len(test_packet)!=1206:
        continue
    for datablock_num in range(0,1):
        db=lidar_parser.get_data_block(test_packet,datablock_num)
        azimuth = lidar_parser.get_azimuth(db)
        print(azimuth)
        if start_azimuth==None:
            start_azimuth = azimuth
            continue
        if 0<start_azimuth-azimuth<5:
            one_rotation=True
            break

        ##print('\n\t',"Azimuth for datablock", str(datablock_num), azimuth )
        for laser_num in range(0,32):  #id15 is 0 degrees
            temp = lidar_parser.get_laser_data(db, laser_num)
            distance = temp[0]
            reflectivity = temp[1]
            ##print('\t', 'laser num:',str(laser_num), str(distance)+"mm", ' intensity=',  str(reflectivity))
            point_3D = lidar_parser.create_3d_point(laser_num,distance, azimuth)
            #print(point_3D)
            #lidar_parser.graph_add_point_3D(point_3D[0], point_3D[1], point_3D[2])
            lidar_parser.graph_add_point_3D(point_3D[0], point_3D[1], point_3D[2])

            #lidar_parser.graph_add_point_2D(laser_num,distance)
            #if laser_num==15:
            #    lidar_parser.graph_add_point_2D(azimuth, distance)
    print("packet #:",packet)
    print("took:",time.time() - starttime, "seconds")
lidar_parser.graph_show_3D()
#lidar_parser.graph_show_2D()

