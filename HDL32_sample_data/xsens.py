import serial
import struct
import binascii
import time


try:
    xsens =None
    while not xsens:
        try:
            xsens = serial.Serial('/dev/ttyUSB0', 115200)
        except:
            pass
    print(xsens)
    while True:
        #get preamble
        unknown_byte = None
        while unknown_byte!=250:   #250 (0xFA)
            poss_preamble= xsens.read(1)

            if ord(poss_preamble)==250:
                bid = xsens.read(1)
                mid = xsens.read(1)
                length = xsens.read(1)
                #print poss_preamble, bid, mid, length
                #print repr(poss_preamble), repr(bid), repr(mid), repr(length)
                #print ord(poss_preamble), ord(bid), ord(mid), ord(length)
                #print binascii.hexlify(poss_preamble), binascii.hexlify(bid), binascii.hexlify(mid), binascii.hexlify(length)
                length = int(binascii.hexlify(length),16)  #Pitfall here, dont try to read the hex rep of length (0x18 = 24 base 10)

                break
            #print("looping..")
        data = xsens.read(length)
        roll = struct.unpack('>f', data[0:4])[0]
        pitch = struct.unpack('>f', data[4:8])[0]
        yaw = struct.unpack('>f', data[8:12])[0]
        lat = struct.unpack('>f', data[12:16])[0]
        lon = struct.unpack('>f', data[16:20])[0]
        alt = struct.unpack('>f', data[20:25])[0]
        #{(EulerAngles|Double|NWU, 24 bytes, (Roll:  -4.03858137, Pitch:   0.55696779, Yaw: 130.61109924)),
        #  (AltitudeEllipsoid|Double, 8 bytes, altEllipsoid: -73.06419373),
        # (LatLon|Double, 16 bytes, (lat:  36.59443283, lon: -121.87578583))}
        print "\tRoll:", roll, "\tPitch:", pitch, "\tYaw:",yaw, "\tAlt:", alt, "\tLat", lat, "\tLon", lon

except Exception as e:
    print("Exception", str(e))
    xsens.close()