import serial
import struct

#print("imu test")

orangeIMU = serial.Serial("/dev/ttyUSB0", 115200)


while True:
    input = orangeIMU.read(1)
    #print(ord(input))
    decimalVal = ord(input)  #take the ord of the input to make it a recognizable decimal value

    if decimalVal == 250:
        #Start of a new packet
        packetList = []

        packetList.append(decimalVal)

        bid = ord(orangeIMU.read(1))
        packetList.append(bid)

        mid = ord(orangeIMU.read(1))
        packetList.append(mid)

        length = ord(orangeIMU.read(1))
        packetList.append(length)

        rawData = orangeIMU.read(length)  #data is unord-ed at this point - still in hex
        packetList.append(rawData)

        checkSum = ord(orangeIMU.read(1))
        packetList.append(checkSum)

        #print(packetList)

        dataBytes = packetList[4]
        firstDataByte = dataBytes[0:4]
        secondDataByte = dataBytes[4:8]
        thirdDataByte = dataBytes[8:12]
        fourthDataByte = dataBytes[12:16]
        fifthDataByte = dataBytes[16:20]
        sixthDataByte = dataBytes[20:24]

        roll = struct.unpack('>f', firstDataByte)
        roll = roll[0]

        pitch = struct.unpack('>f', secondDataByte)
        pitch = pitch[0]

        yaw = struct.unpack('>f', thirdDataByte)
        yaw = yaw[0]

        lat = struct.unpack('>f', fourthDataByte)
        lat = lat[0]

        long = struct.unpack('>f', fifthDataByte)
        long = long[0]

        alt = struct.unpack('>f', sixthDataByte)
        alt = alt[0]

        print("Roll:", roll, ", Pitch:", pitch, ", Yaw:", yaw, ", Lat:", lat, ", Long:", long, \
                ", Alt:", alt)






