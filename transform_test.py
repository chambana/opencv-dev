import numpy as np
from math import cos,sin
import math
import pygame
import random


#gksudo python transform_test.py to forward window


#defines
X = 0
Y = 1
ANGLE_RAD = 2
# TODO:  measure in cm and degrees

#X+ is front of vehicle
#Y+ is left side of vehicle
#x,y,angle
sonar_offsets = {'0': (0,10,math.radians(90)),
                 '1': (5,5,math.radians(45)),
                 '2': (10,0,math.radians(0)),
                 '3': (5,-5,math.radians(-45)),
                 '4': (0,-10,math.radians(-90))}





alpha = sonar_offsets['1'][ANGLE_RAD]
x_offset = sonar_offsets['1'][X]
y_offset = sonar_offsets['1'][Y]

Homogeneous_Transform_R_S = np.matrix([[cos(alpha), -sin(alpha), 0, x_offset], \
                                       [sin(alpha), cos(alpha),  0,  y_offset], \
                                       [0,          0,           1,     0], \
                                       [0,          0,           0,     1]])


sample_point_sonar_frame = np.transpose(np.matrix([5, 0, 0, 1]))
print("Homogeneous Transform Matrix")
print(Homogeneous_Transform_R_S)
print("Sample Point")
print(sample_point_sonar_frame)
p_robot_frame = np.dot(Homogeneous_Transform_R_S, sample_point_sonar_frame)

print("Transformed point")
print(p_robot_frame)


#TODO:  Draw points and linearize scatter


width = 640
height = 480

screen = pygame.display.set_mode((width, height))
clock = pygame.time.Clock()
running = True

rover_center = (width/2, width/2)

while running:
    # x = random.randint(0, width-1)
    # y = random.randint(0, height-1)
    red = random.randint(0, 255)
    green = random.randint(0, 255)
    blue = random.randint(0, 255)

    p_robot_frame=[]
    for i in range(0,5):
        alpha = sonar_offsets[str(i)][ANGLE_RAD]
        x_offset = sonar_offsets[str(i)][X]
        y_offset = sonar_offsets[str(i)][Y]
        sample_point_sonar_frame = np.transpose(np.matrix([10+(5*i), 0, 0, 1]))
        Homogeneous_Transform_R_S = np.matrix([[cos(alpha), -sin(alpha), 0, x_offset], \
                                               [sin(alpha), cos(alpha), 0, y_offset], \
                                               [0, 0, 1, 0], \
                                               [0, 0, 0, 1]])

        p_robot_frame.append(np.dot(Homogeneous_Transform_R_S, sample_point_sonar_frame))
        print(i, p_robot_frame[i])
        print("\n\n")

    for pt in p_robot_frame:
        # Robot frame's +X is pyGame's -Y.  Robot frame +Y is pyGame -X.  Adjust values accordingly
        screen.set_at((rover_center[X] + -pt[Y], rover_center[Y] + -pt[X]), (red, green, blue))


    screen.set_at((int(rover_center[X]),int(rover_center[Y])), (0,255,0))
    #screen.set_at((rover_center[X] + p_robot_frame[X], rover_center[Y] + p_robot_frame[Y]), (red, green, blue))

    #screen.set_at((10,30), (255,0,0))

    for event in pygame.event.get():
         if event.type == pygame.QUIT:
             running = False


    pygame.display.flip()
    clock.tick(240)
