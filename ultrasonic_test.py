import RPi.GPIO as GPIO
import time
import numpy as np

#key = sonar 0-5
#value = (Trigger GPIO, Echo GPIO)
sonar_gpio = {'0': (4 , 17), '1': (25, 8), '2': (23 , 24), '3': (12, 16), '4':(19, 26)}

#defines
TRIG = 0
ECHO = 1

#defines
X = 0
Y = 1
ANGLE_DEG = 2
# TODO:  measure in cm and degrees

#sonar_offsets = {'0': (x,y,angle)}


def init_sonars():
    GPIO.setmode(GPIO.BCM)

    for i in sonar_gpio:
        GPIO.setup(sonar_gpio[i][TRIG], GPIO.OUT)
        GPIO.setup(sonar_gpio[i][ECHO], GPIO.IN)
        GPIO.output(sonar_gpio[i][TRIG], False)
        print "Waiting For Sensor",str(i), "To Settle"
        time.sleep(2)

def ping_sonars():

    for i in sorted(sonar_gpio):

        #Trigger the sonar chirp
        GPIO.output(sonar_gpio[i][TRIG], True)
        time.sleep(0.00001)
        GPIO.output(sonar_gpio[i][TRIG], False)

        while GPIO.input(sonar_gpio[i][ECHO]) == 0:
            pulse_start = time.time()

        while GPIO.input(sonar_gpio[i][ECHO]) == 1:
            pulse_end = time.time()

        pulse_duration = pulse_end - pulse_start

        distance = pulse_duration * 17150

        distance = round(distance, 2)

        print "Distance",str(i)+':', distance, "cm"

if __name__ == "__main__":


    init_sonars()
    try:
        while 1:
            ping_sonars()
            time.sleep(.5)
    except:
        GPIO.cleanup()




