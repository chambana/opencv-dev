from ImageProcessing import *
import unittest
import time
import cv2

class MyTestCase(unittest.TestCase):
    def test_plate_finder(self):
        #image_processor_object = ImageProcessing('./data/redball.avi')
        # image_processor_object = ImageProcessing('./data/forward_test.png')
        # image_processor_object.get_orientation(display_frame=True)
        # print(image_processor_object.orientation, image_processor_object.bearing)
        # self.assertEqual(image_processor_object.orientation, "FORWARD")

        image_processor_object = ImageProcessing('./data/greenredplates2.png')
        image_processor_object.get_orientation(display_frame=True)
        print(image_processor_object.orientation, image_processor_object.bearing)
        self.assertEqual(image_processor_object.orientation, "FORWARD")


    def test_template(self):
        NPS_image_processor = ImageProcessing("_______________")

        starttime = time.time()
        while time.time() - starttime:

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        self.assertEqual()

if __name__ == '__main__':
    unittest.main()