import RPi.GPIO as GPIO
import time
import sys, termios, fcntl, os


fd = sys.stdin.fileno()
oldterm = termios.tcgetattr(fd)
oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)


SERVO1_PIN = 17
SERVO2_PIN= 27

PIN = 18
PWMA1 = 6
PWMA2 = 13
PWMB1 = 20
PWMB2 = 21
D1 = 12
D2 = 26
PWM = 50

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(PIN, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(PWMA1, GPIO.OUT)
GPIO.setup(PWMA2, GPIO.OUT)
GPIO.setup(PWMB1, GPIO.OUT)
GPIO.setup(PWMB2, GPIO.OUT)
GPIO.setup(D1, GPIO.OUT)
GPIO.setup(D2, GPIO.OUT)
p1 = GPIO.PWM(D1, 500)
p2 = GPIO.PWM(D2, 500)
p1.start(50)
p2.start(50)



GPIO.setup(SERVO1_PIN, GPIO.OUT)
GPIO.setup(SERVO2_PIN, GPIO.OUT)
servo1 = GPIO.PWM(SERVO1_PIN,100)
servo2 = GPIO.PWM(SERVO2_PIN,100)
servo1.start(5)
servo2.start(5)

def turn_left():
    servo1.ChangeDutyCycle(5)
    servo2.ChangeDutyCycle(5)


def turn_right():
    servo1.ChangeDutyCycle(25)
    servo2.ChangeDutyCycle(25)


def turn_center():
    servo1.ChangeDutyCycle(12)
    servo2.ChangeDutyCycle(12)


def set_motor(A1, A2, B1, B2):
    GPIO.output(PWMA1, A1)
    GPIO.output(PWMA2, A2)
    GPIO.output(PWMB1, B1)
    GPIO.output(PWMB2, B2)


def forward():
    GPIO.output(PWMA1, 1)
    GPIO.output(PWMA2, 0)
    GPIO.output(PWMB1, 1)
    GPIO.output(PWMB2, 0)


def stop():
    set_motor(0, 0, 0, 0)


def reverse():
    set_motor(0, 1, 0, 1)


def left():
    set_motor(1, 0, 0, 0)


def right():
    set_motor(0, 0, 1, 0)


import sys, termios, fcntl, os



def get_char_keyboard_nonblock():
    try:
        # fd = sys.stdin.fileno()

        # oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)

        # oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

        c = None

        try:
            c = sys.stdin.read(1)
        except IOError:
            pass

        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)

        return c
    except:
        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
        quit()


# def getkey():
# 	if GPIO.input(PIN) == 0:
# 		count = 0
# 		while GPIO.input(PIN) == 0 and count < 200:  #9ms
# 			count += 1
# 			time.sleep(0.00006)
#
# 		count = 0
# 		while GPIO.input(PIN) == 1 and count < 80:  #4.5ms
# 			count += 1
# 			time.sleep(0.00006)
#
# 		idx = 0
# 		cnt = 0
# 		data = [0,0,0,0]
# 		for i in range(0,32):
# 			count = 0
# 			while GPIO.input(PIN) == 0 and count < 15:    #0.56ms
# 				count += 1
# 				time.sleep(0.00006)
#
# 			count = 0
# 			while GPIO.input(PIN) == 1 and count < 40:   #0: 0.56ms
# 				count += 1                               #1: 1.69ms
# 				time.sleep(0.00006)
#
# 			if count > 8:
# 				data[idx] |= 1<<cnt
# 			if cnt == 7:
# 				cnt = 0
# 				idx += 1
# 			else:
# 				cnt += 1
# 		if data[0]+data[1] == 0xFF and data[2]+data[3] == 0xFF:  #check
# 			return data[2]

print('APRS ...')
stop()

# forward()
try:

    while True:
        c = get_char_keyboard_nonblock()
        if c != None:
            if ord(c) == ord('s'):
                print("\nCOMMAND STOP")
                stop()
            if ord(c) == ord('f'):
                print("\nCOMMAND FORWARD")
                forward()
            if ord(c) == ord('b'):
                print("\nCOMMAND BACKWARD")
                reverse()
            if ord(c) == ord('l'):
                print("\nCOMMAND STEER LEFT")
                turn_left()
            if ord(c) == ord('r'):
                print("\nCOMMAND STEER RIGHT")
                turn_right()
            if ord(c) == ord('c'):
                print("\nCOMMAND STEER CENTER")
                turn_center()

except KeyboardInterrupt:
    GPIO.cleanup();
