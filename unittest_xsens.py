'''
Name -- Drew Watson, Liz Wanic, Damon Alcorn
Date -- 5 Dec 2016
Description -- File contains unit testing of specific functions.  This file is used in conjunction with the custom
tools built to test the functionality of the xSens sender and receiver
Filename -- unittest_xsens.py
'''


import unittest
from xSens import *
import binascii

class Test_IMU_module(unittest.TestCase):

    def setUp(self):
        self.senderInstance = xSens_Sender(None, None, None, False, True)

    def tearDown(self):
        self.senderInstance = None

    def test_readPacket(self):
        self.assertEqual(self.senderInstance.readPacket(binascii.unhexlify('3d51f3d63c7239e1411d53f6bb34fc2a3cfd7ae33c69d0f0bf3714ed3e15002abfab06363fd8862340508d0c420d541a7058')),
                         {'yaw': 9.832998275756836, 'lon': 0.030942386016249657, 'pitch': 0.014784307219088078,
                           'lat': -0.002761612180620432, 'alt': 0.014271005988121033, 'roll': 0.05125793069601059})
        self.assertEqual(self.senderInstance.readPacket(binascii.unhexlify('3d6487ad3d4875ba411ca108bbf8e5353cf36ce93b2d3ebebf36f00f3e135f9cbfaa9b933fd81a1a40503954420d2ff47057')),
                         {'yaw': 9.789314270019531, 'lon': 0.02971501834690571, 'pitch': 0.048940397799015045,
                          'lat': -0.007595682982355356, 'alt': 0.0026435102336108685, 'roll': 0.05579345300793648})
        self.assertEqual(self.senderInstance.readPacket(binascii.unhexlify('3d64a44c3d59b506411da0debc637d453ca8f6fa3c89ebabbf3758db3e150bf0bfab3cfb3fd81559404fede2420d12a77056')),
                         {'yaw': 9.851774215698242, 'lon': 0.020625580102205276, 'pitch': 0.05315115302801132,
                          'lat': -0.013884847052395344, 'alt': 0.016836008056998253, 'roll': 0.05582074820995331})

if __name__ == '__main__':
    unittest.main()