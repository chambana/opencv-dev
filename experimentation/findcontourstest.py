import numpy as np
import cv2
from ImageProcessing import ImageProcessing
from config import *

cap = cv2.VideoCapture(0)
NPS_image_processor = ImageProcessing()


while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    #region_of_interest = image_processor.create_region_of_interest(frame, percentage_of_frame=0.5)
    #cv2.imshow("region of interest window", region_of_interest)


    hsvFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    cv2.medianBlur(hsvFrame, BLUR_KERNEL_SIZE, hsvFrame)

    green_mask = NPS_image_processor.create_green_mask(hsvFrame)
    red_mask = NPS_image_processor.create_red_mask(hsvFrame)

    kernel = np.ones((5, 5), np.uint8)
    green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_OPEN, kernel)
    red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_OPEN, kernel)

    green_mask_and_frame = cv2.bitwise_and(frame, frame, mask=green_mask)
    red_mask_and_frame = cv2.bitwise_and(frame, frame, mask=red_mask)

    green_and_red_masks_and_frame = cv2.bitwise_or(green_mask_and_frame, red_mask_and_frame)

    newframe = frame.copy()
    green_with_contours, green_plate_center, green_plate_radius = NPS_image_processor.find_plate(mask=green_mask, orig_frame=frame)
    red_with_contours, red_plate_center, red_plate_radius = NPS_image_processor.find_plate(mask=red_mask, orig_frame=newframe)


    blendtest = cv2.bitwise_or(green_with_contours, red_with_contours)
    NPS_image_processor.draw_plate(blendtest,green_plate_center, green_plate_radius,(0,255,0))
    NPS_image_processor.draw_plate(blendtest,red_plate_center, red_plate_radius,(0,0,255))


    cv2.imshow("blend test", blendtest)
    cv2.imshow("green with contours", green_with_contours)
    cv2.imshow('red with contours', red_with_contours)


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

