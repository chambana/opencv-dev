import numpy as np
import cv2
import time

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    plate_image = cv2.imread('greenredplates.png')

    # Our operations on the frame come here
#    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    plate_image = cv2.cvtColor(plate_image, cv2.COLOR_BGR2HSV)

    #red_lower = np.array([0, 0, 79], dtype ="uint8")
    #red_upper = np.array([40, 40, 191], dtype ="uint8")



    #hsv_green = np.array([60, 255, 255], dtype ="uint8")
    color = 60
    sensitivity = 15
    #green = 60;
    #blue = 120;
    #yellow = 30;

    lower_red_0 = np.array([0, 100, 100])
    upper_red_0 = np.array([sensitivity, 255, 255])
    lower_red_1 = np.array([180 - sensitivity, 100, 100])
    upper_red_1 = np.array([180, 255, 255])



    green_lower = np.array([color - sensitivity, 100, 100])
    green_upper = np.array([color + sensitivity, 255, 255])
    green_mask = cv2.inRange(hsv, green_lower, green_upper)
    green_plate_image_mask = cv2.inRange(plate_image, green_lower,green_upper)
    cv2.imshow('green mask on image', green_plate_image_mask)

    mask_0 = cv2.inRange(hsv, lower_red_0, upper_red_0)
    mask_1 = cv2.inRange(hsv, lower_red_1, upper_red_1)
    red_range = cv2.bitwise_or(mask_1, mask_0)
#    red_plate_image_mask = cv2.inRange(plate_image,)

    # Set the lower and the upper limits for the red color. All the BGR values between (0,0,79) and (40,40,191) will be considered red.
    #red_mask = cv2.inRange(frame, red_lower, red_upper)
    #extraction = cv2.bitwise_and(frame, frame, mask = red_mask)

    #erode (removes small noise, but shrinks the blob)
    # kernel = np.ones((5, 5), np.uint8)
    # erosion = cv2.erode(extraction, kernel, iterations=1)
    #
    # #dilate (grow the blob back)
    # dilation = cv2.dilate(erosion, kernel, iterations=1)
    # opening = cv2.morphologyEx(extraction, cv2.MORPH_OPEN, kernel)
    #
    # justdilated =  cv2.dilate(extraction, kernel, iterations=1)



    # Set up the detector with default parameters.
    #detector = cv2.SimpleBlobDetector_create()

    # Detect blobs.
    #keypoints = detector.detect(extraction)

    # Draw detected blobs as green circles.
    # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
    #blobs = cv2.drawKeypoints(extraction, keypoints, np.array([]), (255, 0, 0))

    # Display the resulting frame
    cv2.imshow('frame',frame)
    cv2.imshow('green mask hsv', green_mask)
    #cv2.imshow('red mask',red_mask)
    #cv2.imshow('extracted red', extraction)
    #cv2.imshow('dilated', justdilated)
    #cv2.imshow('eroded+dilated red', dilation)
    #cv2.imshow('opening', opening)
    #cv2.imshow('blob detector', blobs )




    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()