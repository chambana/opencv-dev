import numpy as np
import cv2
import time
from ImageProcessing import *
import os

try:
    #cap = cv2.VideoCapture("../data/samplevid.mp4")
    #print(os.path.abspath("."))
    #quit()
    #image_processor_object = ImageProcessing('./data/samplevid.mp4')
    #image_processor_object = ImageProcessing('./data/redball.avi')
    #image_processor_object = ImageProcessing('./data/greenredplates.png')
    image_processor_object = ImageProcessing('./data/forward_test.png')

    #cap = cv2.VideoCapture("./redball.avi")
    #test=cap.retrieve()
except Exception as e:
    print(str(e))
#
# if not cap.isOpened():
#     print("cant open")
#     quit()


while(True):
    #time.sleep(1/15)

    # Capture frame-by-frame
    #ret, frame = cap.read()

    image_processor_object.get_orientation(display_frame=True)
    print(image_processor_object.orientation, image_processor_object.bearing)


    # When everything done, release the capture

    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    # cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

NPS_image_processor._destroy()
