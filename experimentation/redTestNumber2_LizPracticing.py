import numpy as np
import cv2
import time

#initializing the camera

videoCamera = cv2.VideoCapture(0)


#looping stuff
#infinite loop - throttled by speed of videoCamera (60 frames per second)
while(True):
    ret, capturedFrame = videoCamera.read()

    hsvFrame = cv2.cvtColor(capturedFrame, cv2.COLOR_BGR2HSV)

#making the mask for green (lime green = (80,255,255) in hsv)
    green = 60
    sensitivity = 25

    green_lower = np.array([green - sensitivity, 100, 100])
    green_upper = np.array([green + sensitivity, 255, 255])
    green_mask = cv2.inRange(hsvFrame, green_lower, green_upper)

    red1 = 180
    red2 = 0
    sensitivity_red = 25

    red_top_upper = np.array([red1, 255, 255])
    red_top_lower = np.array([red1 - sensitivity, 100, 100])

    red_bottom_upper = np.array([red2 + sensitivity, 255, 255])
    red_bottom_lower = np.array([red2, 100, 100])

    red_mask_top = cv2.inRange(hsvFrame, red_top_lower, red_top_upper)
    red_mask_bottom = cv2.inRange(hsvFrame, red_bottom_lower, red_bottom_upper)

    red_mask = cv2.bitwise_or(red_mask_bottom, red_mask_top)

    # cv2.imshow("green_maskWindow", green_mask)
    cv2.imshow("red_maskWindow", red_mask)

    cv2.imshow("cameraWindow", capturedFrame)
    # cv2.imshow("cameraHSVWindow", hsvFrame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

#tearing stuff down

videoCamera.release()
cv2.destroyAllWindows()

