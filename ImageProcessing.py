import numpy as np
import cv2
from config import *
import math


class ImageProcessing(object):

    def __init__(self, camera_number = 0):
        self.camera = cv2.VideoCapture(camera_number)

        self.green_mask = None
        self.red_mask = None
        self.orientation = DIRECTION['UNKNOWN_YAW']
        self.bearing = None

    def _destroy(self):
        self.camera.release()
        cv2.destroyAllWindows()

    def _get_frame(self):
        ret, frame = self.camera.read()
        return frame

    def _create_green_mask(self, frame, green_hue_value = GREEN_HUE, sensitivity = GREEN_SENSITIVITY):
        green_lower = np.array([green_hue_value - sensitivity, 100, 100])
        green_upper = np.array([green_hue_value + sensitivity, 255, 255])
        green_mask = cv2.inRange(frame, green_lower, green_upper)

        self.green_mask = green_mask
        return green_mask

    def _create_red_mask(self, frame, red1_hue_value =RED_HUE_UPPER_RANGE, red2_hue_value =RED_HUE_LOWER_RANGE, \
                        sensitivity =RED_SENSITIVITY):

        red_top_upper = np.array([red1_hue_value, 255, 255])
        red_top_lower = np.array([red1_hue_value - sensitivity, 100, 100])

        red_bottom_upper = np.array([red2_hue_value + sensitivity, 255, 255])
        red_bottom_lower = np.array([red2_hue_value, 100, 100])

        red_mask_top = cv2.inRange(frame, red_top_lower, red_top_upper)
        red_mask_bottom = cv2.inRange(frame, red_bottom_lower, red_bottom_upper)

        red_mask = cv2.bitwise_or(red_mask_bottom, red_mask_top)

        self.red_mask = red_mask
        return red_mask


    def _create_region_of_interest(self, frame, percentage_of_frame = REGION_OF_INTEREST):

        region_of_interest = frame[int(WIDTH_FRAME * (0.5 - 0.5 * percentage_of_frame)): \
            int(WIDTH_FRAME * (0.5 + 0.5 * percentage_of_frame)), 0:WIDTH_FRAME]
        return region_of_interest



    def _find_plate(self, mask, orig_frame=None):

        im2, contours_all, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        #DEBUG
        # if contours_all:
        #     for cont in contours_all:
        #         test2 = cv2.convexHull(cont)
        #         orig_frame = cv2.drawContours(orig_frame, cont, -1, (255, 0, 0), 5)
        #
        #         orig_frame = cv2.drawContours(orig_frame, test2, -1, (255, 255, 255), 5)
        #
        #     cv2.imshow("test2", orig_frame)

        contours_greater_min_area = []
        contours_less_min_area = []
        biggest_contour = ()
        img_w_contours = None

        if contours_all:
            biggest_contour = (0, cv2.contourArea(contours_all[0]))

        index = 0
        for cont in contours_all:
            moments = cv2.moments(cont)
            cx = int(moments['m10'] / moments['m00'])
            cy = int(moments['m01'] / moments['m00'])
            area = cv2.contourArea(cont)
            if area > MIN_CONTOUR_AREA:
                contours_greater_min_area.append(cont)
                if orig_frame!=None:
                    cv2.putText(orig_frame, "area=" + str(area), (cx, cy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
            else:
                contours_less_min_area.append(cont)
                if orig_frame!=None:
                    cv2.putText(orig_frame, "area=" + str(area), (cx, cy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
            if area > biggest_contour[1]:
                biggest_contour = (index, area)
            index += 1

        if orig_frame!=None:
            img_w_contours = cv2.drawContours(orig_frame, contours_greater_min_area, -1, (0, 255, 0), 3)
            img_w_contours = cv2.drawContours(img_w_contours, contours_less_min_area, -1, (0, 0, 255), 3)

        center = None
        radius = None
        if biggest_contour and biggest_contour[1]>=MIN_CONTOUR_AREA:
            (x, y), radius = cv2.minEnclosingCircle(contours_all[biggest_contour[0]])
            center = (int(x), int(y))
            radius = int(radius)

            if orig_frame!=None:
                img_w_contours = cv2.drawContours(img_w_contours, contours_all, biggest_contour[0], (255, 0, 0), 5)
                cv2.circle(img_w_contours, center, radius, (255, 255, 0), 2)

        return img_w_contours, center, radius


    def _draw_plate(self, frame, center, radius, color):
        if center!=None and radius!=None:
            cv2.circle(frame, center, radius, color=color, thickness=-1)


    def _calculate_orientation(self, red_plate_center, green_plate_center, draw_overlay_text = True):

        #CASE 1:  green on right, red on left    (FORWARD, confidence=100%)
        #CASE 2:  green on right, red is None    (FORWARD, confidence=50%)
        #CASE 3:  green is None,  red on left    (FORWARD, confidence=50%)
        #CASE 4:  green on left,  red on right   (BACKWARD, confidence=100%)
        #CASE 5:  green on left,  red is None    (BACKWARD, confidence=50%)
        #CASE 6:  green is None,  red on right   (BACKWARD, confidence=50%)
        #CASE 7:  green on right, red on right   (YAW'd LEFT, confidence=50%)
        #CASE 8:  green on left,  red on left    (YAW'd RIGHT, confidence=50%)
        #CASE 9:  green is None, red is None     (????, confidence=0%)

        midpoint_x_axis = WIDTH_FRAME/2
        greenplate_center_x_axis = None if green_plate_center==None else green_plate_center[0]
        redplate_center_x_axis = None if red_plate_center==None else red_plate_center[0]
        estimated_bearing_degrees = None
        orientation = None


        if greenplate_center_x_axis!=None and redplate_center_x_axis!=None:

            if greenplate_center_x_axis > midpoint_x_axis   and   redplate_center_x_axis < midpoint_x_axis:
            #CASE 1:  green on right, red on left    (FORWARD, confidence=100%)
                plate_delta_midpoint = ((greenplate_center_x_axis - redplate_center_x_axis)/2) + redplate_center_x_axis
                angle = math.atan2(midpoint_x_axis - plate_delta_midpoint, red_plate_center[1])
                estimated_bearing_degrees = math.degrees(angle)
                orientation = DIRECTION["FORWARD_YAW"]


            elif greenplate_center_x_axis < midpoint_x_axis   and   redplate_center_x_axis > midpoint_x_axis:
            #CASE 4:  green on left,  red on right   (BACKWARD, confidence=100%)
                orientation = DIRECTION['BACKWARD_YAW']

            elif greenplate_center_x_axis > midpoint_x_axis   and   redplate_center_x_axis > midpoint_x_axis:
            #CASE 7:  green on right, red on right   (YAW'd LEFT, confidence=50%)
                orientation = DIRECTION['LEFT_YAW']

            elif greenplate_center_x_axis < midpoint_x_axis   and   redplate_center_x_axis < midpoint_x_axis:
            #CASE 8:  green on left,  red on left    (YAW'd RIGHT, confidence=50%)
                orientation = DIRECTION['RIGHT_YAW']


        elif greenplate_center_x_axis!=None and redplate_center_x_axis==None:

            if greenplate_center_x_axis > midpoint_x_axis:
            #CASE 2:  green on right, red is None    (FORWARD, confidence=50%)
                orientation = DIRECTION['FORWARD_YAW']

            elif greenplate_center_x_axis < midpoint_x_axis:
            #CASE 5:  green on left,  red is None    (BACKWARD, confidence=50%)
                orientation = DIRECTION['BACKWARD_YAW']


        elif greenplate_center_x_axis==None and redplate_center_x_axis!=None:

            if redplate_center_x_axis < midpoint_x_axis:
            #CASE 3:  green is None,  red on left    (FORWARD, confidence=50%)
                orientation = DIRECTION['FORWARD_YAW']

            elif redplate_center_x_axis > midpoint_x_axis:
            #CASE 6:  green is None,  red on right   (BACKWARD, confidence=50%)
                orientation = DIRECTION['BACKWARD_YAW']

        elif greenplate_center_x_axis==None   and   redplate_center_x_axis==None:
        #CASE 9:  green is None, red is None     (????, confidence=0%)
            orientation = DIRECTION['UNKNOWN_YAW']


        else:
        #catch all, zero confidence
            orientation = DIRECTION['UNKNOWN_YAW']

        self.orientation = orientation
        self.bearing = estimated_bearing_degrees
        return estimated_bearing_degrees, orientation


    def _draw_orientation(self, input_frame):

        midpoint_frame = int(WIDTH_FRAME / 2)

        if self.orientation==DIRECTION['FORWARD_YAW'] and self.bearing!=None:

            x_offset = math.tan(math.radians(self.bearing)) * int(HEIGHT_FRAME * 0.8)
            cv2.arrowedLine(input_frame, (midpoint_frame, int(HEIGHT_FRAME * 0.9)),
                        (int(midpoint_frame - x_offset), int(HEIGHT_FRAME * 0.1)), (0, 255, 0), 10)
            cv2.putText(input_frame, "Bearing: "+str(int(self.bearing))+"  Orientation: FORWARD YAW", (int(WIDTH_FRAME / 2 * 0.5), 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), thickness=2)
        else:
            cv2.putText(input_frame, "Orientation: "+str(self.orientation), (int(WIDTH_FRAME / 2 * 0.5), 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), thickness=2)


    def get_orientation(self, display_frame = False):

        #grab a single frame
        frame = self._get_frame()

        #convert from BlueGreenRed colorspace to HueSaturationValue colorspace (because stuff)
        hsvFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        #blur the frame to make the blobs more uniform
        cv2.medianBlur(hsvFrame, BLUR_KERNEL_SIZE, hsvFrame)

        #create a green and a red mask
        self.green_mask = self._create_green_mask(hsvFrame)
        self.red_mask = self._create_red_mask(hsvFrame)

        #Erode and Dilate by using morphologyEx.  Erode eliminates small noise (but shrinks your target blob)
        #Dilate will then re-grow the blob to help account for the shrinkage.
        kernel = np.ones((ERODE_DILATE_KERNAL_SIZE,ERODE_DILATE_KERNAL_SIZE), np.uint8)
        self.green_mask = cv2.morphologyEx(self.green_mask, cv2.MORPH_OPEN, kernel)
        self.red_mask = cv2.morphologyEx(self.red_mask, cv2.MORPH_OPEN, kernel)

        #create deep copy of frame
        newframe = frame.copy()

        #find the green and red plates amongst the various contours in the frame
        green_with_contours, green_plate_center, green_plate_radius = self._find_plate(mask=self.green_mask,orig_frame=frame)
        red_with_contours, red_plate_center, red_plate_radius = self._find_plate(mask=self.red_mask, orig_frame=newframe)

        #figure out if we are facing FORWARD, LEFT, RIGHT, BACKWARD (or UNKNOWN) direction
        self._calculate_orientation(red_plate_center, green_plate_center)

        #For debugging, display the frame with overlay
        if display_frame:
            blendtest = cv2.bitwise_or(green_with_contours, red_with_contours)

            self._draw_plate(blendtest, green_plate_center, green_plate_radius, (0, 255, 0))
            self._draw_plate(blendtest, red_plate_center, red_plate_radius, (0, 0, 255))

            self._draw_orientation(blendtest)
            cv2.imshow("blend test", blendtest)

        return self.orientation


if __name__ == '__main__':

    NPS_image_processor = ImageProcessing(camera_number=0)


    while (True):

        NPS_image_processor.get_orientation(display_frame=True)
        print(NPS_image_processor.orientation, NPS_image_processor.bearing)


        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    NPS_image_processor._destroy()








################################### OLD CODE, SCRATCHPAD #########################################

    # Capture frame-by-frame
    # ret, frame = cap.read()


    # hsvFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # cv2.medianBlur(hsvFrame, BLUR_KERNEL_SIZE, hsvFrame)
    #
    # green_mask = NPS_image_processor.create_green_mask(hsvFrame)
    # red_mask = NPS_image_processor.create_red_mask(hsvFrame)q
    #
    # kernel = np.ones((5, 5), np.uint8)
    # green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_OPEN, kernel)
    # red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_OPEN, kernel)
    #
    # green_mask_and_frame = cv2.bitwise_and(frame, frame, mask=green_mask)
    # red_mask_and_frame = cv2.bitwise_and(frame, frame, mask=red_mask)
    #
    # green_and_red_masks_and_frame = cv2.bitwise_or(green_mask_and_frame, red_mask_and_frame)
    #
    # newframe = frame.copy()
    # _, green_plate_center, green_plate_radius = NPS_image_processor.find_plate(mask=green_mask)
    #
    # NPS_image_processor.draw_plate(newframe, green_plate_center, green_plate_radius, (0,255,0))
    # cv2.imshow('just green plate', newframe)
    #
    # green_with_contours, green_plate_center, green_plate_radius = NPS_image_processor.find_plate(mask=green_mask,\
    #                                                                                              orig_frame=frame)
    # red_with_contours, red_plate_center, red_plate_radius = NPS_image_processor.find_plate(mask=red_mask,\
    #                                                                                        orig_frame=newframe)
    # bearing, orientation = NPS_image_processor.calculate_orientation(red_plate_center, green_plate_center)
    #
    # blendtest = cv2.bitwise_or(green_with_contours, red_with_contours)
    #
    # NPS_image_processor.draw_plate(blendtest, green_plate_center, green_plate_radius, (0, 255, 0))
    # NPS_image_processor.draw_plate(blendtest, red_plate_center, red_plate_radius, (0, 0, 255))
    #
    # NPS_image_processor.draw_orientation(blendtest)
    # cv2.imshow("blend test", blendtest)
    # cv2.imshow("green with contours", green_with_contours)
    # cv2.imshow('red with contours', red_with_contours)