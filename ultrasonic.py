import numpy as np
from math import cos,sin
import math
import pygame
import random
import RPi.GPIO as GPIO
import time

#INSTRUCTIONS FOR USING VIA SSH
#ssh using x forwarding:  ssh -X pi@168.192.__.__
#run using gksudo python ultrasonic.py  to forward the pygame window


#TODO:  measure locations of sonars in cm and get angles

#defines
X = 0
Y = 1
ANGLE_RAD = 2
WIDTH = 640
HEIGHT = 480
TRIG = 0
ECHO = 1



class Ultrasonic(object):
    def __init__(self):
        # X+ is front of vehicle
        # Y+ is left side of vehicle
        # x,y,angle
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.sonar_gpio = {'0': (4, 17), '1': (25, 8), '2': (23, 24), '3': (12, 16), '4': (19, 26)}
        self.sonar_offsets = {'0': (0,10,math.radians(90)),
                 '1': (5,5,math.radians(45)),
                 '2': (10,0,math.radians(0)),
                 '3': (5,-5,math.radians(-45)),
                 '4': (0,-10,math.radians(-90))}

        self.point_cloud_world_frame = []  #measured from origin

        self.init_sonars()

    def transform_point_to_robot_frame(self, point_sonar_frame, sonar_number):
        try:
            x_offset = self.sonar_offsets[str(sonar_number)][X]
            y_offset = self.sonar_offsets[str(sonar_number)][Y]
            alpha = self.sonar_offsets[str(sonar_number)][ANGLE_RAD]
            #print(x_offset,y_offset,alpha)
            Homogeneous_Transform_O_R = np.matrix([[cos(alpha),-sin(alpha), 0,     x_offset],
                                                  [sin(alpha), cos(alpha),  0,     y_offset],
                                                  [0,          0,           1,     0],
                                                  [0,          0,           0,     1]  ])

            point_sonar_frame_transposed = np.transpose(np.matrix([point_sonar_frame[X],0, 0, 1]))
            point_robot_frame = np.dot(Homogeneous_Transform_O_R, point_sonar_frame_transposed)
            #print(points_robot_frame)
            return point_robot_frame
        except Exception as e:
            print("exception in tranform point", str(e))
            return None

    def transform_point_to_world_frame(self, point_robot_frame):
        pass

    def draw_points(self):
        pass
        #clock = pygame.time.Clock()


    def init_sonars(self):
        GPIO.setmode(GPIO.BCM)

        for i in self.sonar_gpio:
            GPIO.setup(self.sonar_gpio[i][TRIG], GPIO.OUT)
            GPIO.setup(self.sonar_gpio[i][ECHO], GPIO.IN)
            GPIO.output(self.sonar_gpio[i][TRIG], False)
            print("Waiting For Sensor",str(i), "To Settle")
            time.sleep(2)

    def ping_sonars(self):
        sonar_distance_readings=[]
        for i in sorted(self.sonar_gpio):
            #Trigger the sonar chirp
            GPIO.output(self.sonar_gpio[i][TRIG], True)
            time.sleep(0.00001)
            GPIO.output(self.sonar_gpio[i][TRIG], False)
            while GPIO.input(self.sonar_gpio[i][ECHO]) == 0:
                pulse_start = time.time()
            while GPIO.input(self.sonar_gpio[i][ECHO]) == 1:
                pulse_end = time.time()
            pulse_duration = pulse_end - pulse_start
            distance = pulse_duration * 17150
            distance = round(distance, 2)
            print("Distance",str(i)+':', distance, "cm")
            sonar_distance_readings.append(distance)
        return sonar_distance_readings

    def cleanup(self):
        GPIO.cleanup()

if __name__ == "__main__":

    sonar_obj = Ultrasonic()
    try:
        clock = pygame.time.Clock()

        running=True
        while(running):
            distance_readings_sonar_frame = sonar_obj.ping_sonars()
            points_robot_frame=[]
            for index,pt in enumerate(distance_readings_sonar_frame):
                #print(index,pt)
                points_robot_frame.append(sonar_obj.transform_point_to_robot_frame([pt],index))
                #print(points_robot_frame)
                red = random.randint(0, 255)
                green = random.randint(0, 255)
                blue = random.randint(0, 255)

                # Robot frame's +X is pyGame's -Y.  Robot frame +Y is pyGame -X.  Adjust values accordingly
                #sonar_obj.screen.set_at((WIDTH / 2 + -points_robot_frame[-1][Y], HEIGHT / 2 + -points_robot_frame[-1][X]), (red, green,blue))
                pygame.draw.circle(sonar_obj.screen, (red,green, blue), (WIDTH / 2 + -points_robot_frame[-1][Y], HEIGHT / 2 + -points_robot_frame[-1][X]), 5, 0)
                print(index,pt)

                #sample_point_r_frame = np.transpose(np.matrix([5, 0, 0, 1]))
                #sonar_obj.screen.set_at((rover_center[X] + p_world_frame[X], rover_center[Y] + p_world_frame[Y]), (red, green, blue))

            print("about to flip")
            pygame.display.flip()
            clock.tick(240)
            time.sleep(0.2)
            sonar_obj.screen.fill((0, 0, 0))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False


    except Exception as e:
        print(str(e))
        print("cleaning up")
        sonar_obj.cleanup()

    print("cleaing up")
    sonar_obj.cleanup()
    print("done")




