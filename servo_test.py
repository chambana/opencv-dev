import RPi.GPIO as GPIO
import time
import curses


GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
pwm = GPIO.PWM(23, 100)
pwm.start(5)




def main(stdscr):
    # do not wait for input when calling getch
    stdscr.nodelay(1)
    angle = 0

    while True:
        # get keyboard input, returns -1 if none available
        c = stdscr.getch()
        stdscr.refresh()
        #     # return curser to start position
        stdscr.move(0, 0)
        # if c != -1:
        #     # print numeric value
        #     #stdscr.addstr(str(c) + ' ')
        #     #stdscr.refresh()
        #     # return curser to start position
        #     #stdscr.move(0, 0)
        #     pass

        if c==260:  #left arrow
            #print(str(c))
            angle = 0
        if c==261:   #right arrow
            angle = 210
        print(angle)

        duty = float(angle) / 10.0 + 2.5
        #print("duty", duty)
        pwm.ChangeDutyCycle(duty)

        #angle += 10
        time.sleep(.2)


if __name__ == '__main__':
    curses.wrapper(main)